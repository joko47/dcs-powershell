﻿[cmdletbinding()]
param
    (
        [switch] $CopyToUSB,
        [switch] $EnableScripts,
        [switch] $CreateNewFolder,
        [string] $FolderName

    )

$CurrentPath = Get-Location
$ActiveUser = $env:USERNAME
$PowershellPath = "C:\Users\" + $ActiveUser + "\Documents"

if($CopyToUSB)
{
    Write-Host "Copy to USB"
    if($CreateNewFolder)
    {
        
        $CurrentPath = Join-Path -Path $CurrentPath -ChildPath $FolderName
        Write-Host "Create New Folder $CurrentPath"
        if (!(Test-Path -Path $CurrentPath))
        {
            Write-Host "Folder does not exist"
            [void](New-Item -ItemType directory -Path $CurrentPath)
            Write-Host $CurrentPath
        }
    }
}
else
{
    Write-Host "Copy to Powershell folder"
    if($CreateNewFolder)
    {
        $FullPowershellPath = Join-Path -Path $FullPowershellPath -ChildPath $FolderName
        if (Test-Path -Path $FullPowershellPath)
        {
            [void](New-Item -ItemType directory -Path $FullPowershellPath)
        }
    }
}

$ActiveUser = $env:USERNAME
$PowershellPath = "C:\Users\" + $ActiveUser + "\Documents"
$FullPowershellPath = $PowershellPath + "\WindowsPowershell"
$CurrentPath = join-path -Path $CurrentPath -childPath WindowsPowerShell

Write-Host "Copy $CopyToUSB"
Write-Host "Create Folder $CreateNewFolder"
Write-Host "Name $FolderName"


#Check if folders/files exist
if(!(Test-Path -Path $FullPowershellPath))
{
    Write-Host "Powershell Path does not exist"
    if(!$CopyToUSB)
    {
        Write-Host "Creating: " $FullPowershellPath
        [void](New-Item -ItemType directory -Path $FullPowershellPath)
    }
}


if($CopyToUSB)
{
    #Check if folders/files exist
    $SourceFiles = Get-ChildItem -Recurse -Path $FullPowershellPath
    $SourceFolder = $FullPowershellPath
    $DestinationFiles = Get-ChildItem -Recurse -Path $CurrentPath
    $DestinationFolder = $CurrentPath
}
else
{
    $DestinationFiles = Get-ChildItem -Recurse -Path $FullPowershellPath
    $DestinationFolder = $FullPowershellPath
    $SourceFiles = Get-ChildItem -Recurse -Path $CurrentPath
    $SourceFolder = $CurrentPath

}

Write-Host "Source $SourceFiles"
Write-Host "Destination $DestinationFiles"




if($DestinationFiles -ne $null){
    Write-Host "Files exist in destination folder"
    Write-Host "Finding differences..."
    $DifferenceObject = (Compare-Object -ReferenceObject $SourceFiles -DifferenceObject $DestinationFiles -Property "FullName","LastWriteTime" -PassThru)
    foreach($object in $DifferenceObject){
        
        if($object.FullName.contains($CurrentPath)){
            Write-Host "Checking if " $object.FullName " Exists in destination folder"
            $src = $Object
            $Dest = $DifferenceObject | Where-Object{$_.Name -eq $src.Name -and $_.FullName.contains($FullPowershellPath)}

            #Write-Host "source: " $src.FullName
            #Write-Host "Destination: " $Dest.FullName
            
            if($Dest -eq $null)
            {
                Write-Host "Does not exist in destination folder, copying file..."
                Copy-Item -Path $src.FullName -Destination $src.FullName.Replace($CurrentPath,$FullPowershellPath)
            }
            Elseif($Dest.FullName -ne $src.FullName.Replace($CurrentPath,$FullPowershellPath))
            {
                Write-Host "Does not exist in destination folder, copying file..."
                Copy-Item -Path $src.FullName -Destination $src.FullName.Replace($CurrentPath,$FullPowershellPath)
            }
            Elseif($src.LastWriteTime -gt $Dest.LastWriteTime)
            {
                Write-Host "File exists in destination, but source is newer, overwriting file..."
                Copy-Item -Force -Path $src.FullName -Destination $Dest.FullName
            }
            Else
            {
                Write-Host $src.Fullname "older than" $Dest.FullName "... Skip copy"
            }
        }
        Else
        {
            Write-Host "Checking if" $Object.FullName "has corrosponding file in source"
            $Dest = $object
            $src = $DifferenceObject | Where-Object{$_.Name -eq $Dest.Name -and $_.FullName.contains($CurrentPath)} 
            if($src -eq $null)
            {
                Write-Host $Dest.FullName "Does not exist in source"
            }
            Elseif($src -ne $Dest.FullName.Replace($FullPowershellPath,$CurrentPath))
            {
                Write-Host $Dest.FullName "Does not exist in source"
            }
        }
    }


       
}

Else
{
    Write-host "No files in destination directory, copying all files"
    #Copy-Item -Recurse .\WindowsPowershell $PowershellPath -ErrorAction SilentlyContinue
    foreach($file in $SourceFiles)
    {
        Copy-Item -Path $file.FullName -Destination $file.FullName.Replace($SourceFolder,$DestinationFolder)
    }
}

Read-Host -Prompt "Press Enter to exit"
