﻿$URL = "https://gitlab.com/joko47/dcs-powershell/-/archive/master/dcs-powershell-master.zip"
$output = Join-Path -Path (Get-Location).Path -ChildPath "\dcs-powershell.zip"
$start_time = Get-Date

Write-Host $output
Invoke-WebRequest -Uri $url -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

Expand-Archive -Path $output -DestinationPath ".\"

Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

Get-ChildItem -Path .\dcs-powershell-master | Move-Item -force -Destination .\

Remove-Item –path $output
Remove-Item -recurse -path .\dcs-powershell-master

Read-Host -Prompt "Press Enter to exit"