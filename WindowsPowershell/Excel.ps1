﻿Param([String[]] $ComputerName = $env:COMPUTERNAME
     )

foreach ($C in $ComputerName){
    if($C -eq $env:COMPUTERNAME){
        $Excel = test-path "C:\Program Files (x86)\Microsoft Office\Office16\EXCEL.EXE"
    }
    Else{
        $Excel = Invoke-Command -ComputerName $C -ScriptBlock {test-path "C:\Program Files (x86)\Microsoft Office\Office16\EXCEL.EXE"}
    }
    Write-Host "$C has Excel: $Excel"
}
