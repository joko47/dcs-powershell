Param([Switch] $IncludeDead)

if($MyPowershellProfile -eq "JakobProfile"){Name "Updating Computerlist - Started"}
Write-Host Updating AD Computers
$ADComputerlist = Invoke-Command -computername ($env:COMPUTERNAME.SubString(0,6) + "DC02") -ScriptBlock{(Get-ADComputer -filter 'Name -like "2031pp*" -and Enabled -eq "true"').Name}

$ComputerList = @()
if ($IncludeDead){
    $ComputerList = $ADComputerList
}
Else
{
    $ping = new-object System.Net.NetworkInformation.Ping

	$ADCount = $ADComputerlist.count

	Write-Host "Finding Online PCs"
	foreach($Computer in $ADComputerlist){
	    $x += 1
	    if($MyPowershellProfile -eq "JakobProfile"){Name "Updating Computerlist - Processing $Computer ($x/$ADCount)"}
	    $Online = $False
	    $Res = $null
	    Try{$Res=$ping.send($Computer,1)}
	    Catch{$Online = $False}
	    if($Res.Status -ne "TimedOut" -and $res -ne $null){$Online = $true}
	    if($Online -eq $true){$ComputerList += $Computer}
    }
}
$Computerlist | Out-File -FilePath $Global:ComputerPath
$Global:ComputerList = $ComputerList
if($MyPowershellProfile -eq "JakobProfile"){Name "Updating Computerlist - Finished"}
