﻿function Get-ProcessCreationTime
{
    [cmdletbinding()]
    param
    (
        $ComputerName=$env:COMPUTERNAME,
        [parameter(Mandatory=$true)]
        $ProcessName,
        [switch]$Naming
    )

    BEGIN
    {
        Function New-Process-Object
        {
            $Obj = New-Object -TypeName PSObject -Property @{
                'CSName' = $null
                'Username' = "No Logged On User"
                'ProcessID' = $null
                'CreationTime' = $null
                'DomainName' = $null
                'ProcessName' = $ProcessName
            }
            $Obj.psobject.typenames.insert(0,'HDObject.ProcessCreationTime')
            Write-Output $Obj
        } # END: Function New-UserSession-Object
        $Objects = @()
        if(($MyPowershellProfile -eq "JakobProfile") -and $Naming){$PSProfileFound = $true}Else{$PSProfileFound = $false}
    } # END: BEGIN Block 
    
    
    PROCESS
    {
        
        $processes = $null
        $Processes = Get-WmiObject -Class Win32_Process -ComputerName $ComputerName -Filter "name='$ProcessName'"
        if($Processes) 
        {
            foreach ($process in $processes) 
            {
            $Obj =  New-Process-Object
            $Obj.ProcessName = $process.caption
            $Obj.ProcessId = $process.handle
            $Obj.CreationTime = $Process.Converttodatetime($Process.creationdate)
            $Obj.UserName = $process.getowner().user
            $Obj.DomainName = $process.getowner().domain
            $Obj.CSName = $process.CSName 
           
            $Objects += $Obj
            }
        } 
        else 
        {
            write-host "`nNo Process found with name $ProcessName on $comp"
        }
    write-Output $Objects
    }
}
