﻿function Set-Shortcut{
    param([String] $ShortcutPath, [String] $SourcePath)
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($ShortcutPath)
    $Shortcut.TargetPath = $SourcePath
    $Shortcut.save()
}