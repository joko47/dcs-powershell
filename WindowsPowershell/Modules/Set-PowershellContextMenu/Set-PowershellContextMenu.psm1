﻿﻿#requires -version x.x

Function Set-PowershellContextMenu {

<#
.SYNOPSIS
Add "Open Powershell Here" and "Open Powershell ADMIN Here" to context menu.

.DESCRIPTION
Manipulates registry keys, to add "Open Powershell Here" and "Open Powershell ADMIN Here" to context menu.
Will check if registry keys are already made, and will not overwrite keys

Requires administrator rights, to manipulate registry keys

.EXAMPLE
PS> Set-PowershellContextMenu

If Powershell context menu does not already exits, creates them

.INPUTS
N/A

.OUTPUTS
N/A

.NOTES
Additional information about the function or script.

.LINK
Get-Help

.ROLE
Administrator

.FUNCTIONALITY
Add "Open Powershell Here" and "Open Powershell ADMIN Here" to context menu.

#>

[CmdletBinding(SupportsShouldProcess)]

If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "Set-PowershellContextMenu"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}

if(!(Test-Path -Path HKCR:\)){New-PSDrive -PSProvider registry -Root HKEY_CLASSES_ROOT -Name HKCR}

$ShellPath = "HKCR:\Directory\shell"
$PowershellMenu = Join-Path -Path $ShellPath -ChildPath "\PowershellMenu"
$PowershellCommand = Join-Path -Path $ShellPath -ChildPath "\PowershellMenu\command"
$PowershellBackground = "HKCR:\Directory\Background\shell"
$PowershellBGMenu = Join-Path -Path $PowershellBackground -ChildPath "\PowershellMenu"
$PowershellBGCommand = Join-Path -Path $PowershellBackground -ChildPath "\PowershellMenu\command"

if(Test-Path -Path $ShellPath){
    if(!(Test-Path -path $PowershellMenu)){
    New-Item -Path $PowershellMenu
    }
    Set-ItemProperty -force -Path $PowershellMenu -Name "(Default)" -Value "Open Powershell Here"
    Set-ItemProperty -Path $PowershellMenu -Name "Icon" -Value C:\Windows\system32\WindowsPowerShell\v1.0\powershell.ico
    if(!(Test-Path -path $PowershellCommand)){
    New-Item -Path $PowershellCommand
    }
    Set-ItemProperty -Path $PowershellCommand -Name "(Default)" -Value "powershell.exe -NoExit -Command Set-Location -LiteralPath '%V'"
    
}
if(Test-Path -Path $PowershellBackground){
    if(!(Test-Path -path $PowershellBGMenu)){
    New-Item -Path $PowershellBGMenu
    }
    Set-ItemProperty -force -Path $PowershellBGMenu -Name "(Default)" -Value "Open Powershell Here"
    Set-ItemProperty -Path $PowershellBGMenu -Name "Icon" -Value C:\Windows\system32\WindowsPowerShell\v1.0\powershell.ico
    if(!(Test-Path -path $PowershellBGCommand)){
    New-Item -Path $PowershellBGCommand
    }
    Set-ItemProperty -Path $PowershellBGCommand -Name "(Default)" -Value "powershell.exe -NoExit -Command Set-Location -LiteralPath '%V'"
    
}

$ShellPath = "HKCR:\Directory\shell"
$PowershellMenu = Join-Path -Path $ShellPath -ChildPath "\PowershellAdmin"
$PowershellCommand = Join-Path -Path $ShellPath -ChildPath "\PowershellAdmin\command"
$PowershellBackground = "HKCR:\Directory\Background\shell"
$PowershellBGMenu = Join-Path -Path $PowershellBackground -ChildPath "\PowershellAdmin"
$PowershellBGCommand = Join-Path -Path $PowershellBackground -ChildPath "\PowershellAdmin\command"
if(Test-Path -Path $ShellPath){
    if(!(Test-Path -path $PowershellMenu)){
    New-Item -Path $PowershellMenu
    }
    Set-ItemProperty -force -Path $PowershellMenu -Name "(Default)" -Value "Open Powershell ADMIN Here"
    Set-ItemProperty -Path $PowershellMenu -Name "Icon" -Value C:\Windows\system32\WindowsPowerShell\v1.0\PowerShell_admin.ico
    Set-ItemProperty -Path $PowershellMenu -Name HasLUAShield -Value ''
    if(!(Test-Path -path $PowershellCommand)){
    New-Item -Path $PowershellCommand
    }
    Set-ItemProperty -Path $PowershellCommand -Name "(Default)" -Value "PowerShell -windowstyle hidden -Command ""Start-Process cmd -ArgumentList '/s,/k,pushd,%V && start PowerShell && exit' -Verb RunAs"
    
}
if(Test-Path -Path $PowershellBackground){
    if(!(Test-Path -path $PowershellBGMenu)){
    New-Item -Path $PowershellBGMenu
    }
    Set-ItemProperty -force -Path $PowershellBGMenu -Name "(Default)" -Value "Open Powershell ADMIN Here"
    Set-ItemProperty -Path $PowershellBGMenu -Name "Icon" -Value C:\Windows\system32\WindowsPowerShell\v1.0\PowerShell_admin.ico
    Set-ItemProperty -Path $PowershellBGMenu -Name HasLUAShield -Value ''
    if(!(Test-Path -path $PowershellBGCommand)){
    New-Item -Path $PowershellBGCommand
    }
    Set-ItemProperty -Path $PowershellBGCommand -Name "(Default)" -Value "PowerShell -windowstyle hidden -Command ""Start-Process cmd -ArgumentList '/s,/k,pushd,%V && start PowerShell && exit' -Verb RunAs"
    
    
}
}
