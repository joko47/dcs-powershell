﻿Function Disconnect-User
{
    [CmdletBinding()]
    Param (
        [parameter(ValueFromPipeline=$true)]
        [String[]]$ComputerName=$env:COMPUTERNAME,
        [parameter(ValueFromPipeline=$true)]
        [String[]]$UserName=$env:UserName,
        [String[]]$Delay=60,
        [String[]]$Message="$env:Username would like to log you off, is it okay?",
        [String[]]$Title="Log off queary",
        [string]$Buttons="YesNo",
        [string]$Icon="Question"
    )


                ## Find all sessions matching the specified username
                $sessions = quser /server $ComputerName | Where-Object {$_ -match $UserName}
                ## Parse the session IDs from the output
                $sessionIds = ($sessions -split ' +')[2]
                Write-Host "Found $(@($sessionIds).Count) user login(s) on computer."
                ## Loop through each session ID and pass each to the logoff command
                $sessionIds | ForEach-Object {
                    Write-Host "Logging off session id [$($_)]..."
                    logoff $_ /server $ComputerName
                }

 }
