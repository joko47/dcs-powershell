﻿Function New-Switch-Object
        {
            Param ([String]$Name,
                   [String]$IP,
                   [Int]$Size,
                   [String]$Location
                  )
            $Obj = New-Object -TypeName PSObject -Property @{
                'Name' = $Name
                'IP' = $IP
                'Online' = $False
                'Size' = $Size
                'State' = $null
                'Location' = $Location
                'Ports' = @()
                }
            $Obj.psobject.typenames.insert(0,'HDObject.Switch')
            
            for($i=1;$i -le $Size+1;$i++){
               $Obj.Ports += New-Port-Object -Port "$i"
            }

            Write-Output $Obj
        } # END: Function New-UserSession-Object ----------------------------------

Function New-Port-Object
        {
            Param([Int]$Port)
            $Obj = New-Object -TypeName PSObject -Property @{
                'Port' = $Port
                'Name' = $null
                'IP' = $null
                'Online' = $False
                'Location' = $null
                'Description' = $null
                'Vlan' = @()
                }
            $Obj.psobject.typenames.insert(0,'HDObject.Port')
            Write-Output $Obj
        } # END: Function New-UserSession-Object ----------------------------------

Function Set-Port
{
    Param ([Object]$Port,
           [String]$Name,
           [String]$IP,
           [String]$Description,
           [String]$Location,
           [String[]]$Vlan
          )
    if($Name -ne $null){$Port.Name = $Name}
    if($IP -ne $null){$Port.IP = $IP}
    if($Description -ne $null){$Port.Description = $Description}
    if($Location -ne $null){$Port.Location = $Location}
    if($Vlan -ne $null){$Port.Vlan = $Vlan}

    Write-Output $Port
}

Function Update-Port{
    Param([Object]$Switch,
          [Int]$PortNumber,
          [String]$Name = $null,
          [String]$IP = $null,
          [String]$Description = $null,
          [String]$Location = $null,
          [String[]]$Vlan = $null
         )

    $Port = $Switch.Ports | Where-Object {$_.Port -eq $PortNumber}
    $Port = Set-Port -Port $Port -Name $Name -IP $IP -Description $Description -Location $Location -Vlan $Vlan
    
    Write-Output $Switch
    
}

Function Get-Port{
    Param([Parameter(HelpMessage="Enter a search criteria")]
          [ValidateNotNullorEmpty()]
          [ValidateSet("PortNumber", "Name", "Location","Description","IP","Vlan")]
          [string]$Type = "PortNumber",
          [string]$Criteria,
          [Object]$Switch
         )

    if ($Type -eq "PortNumber"){
        Write-Output $Switch.Ports | Where-Object {$_.Port -eq $Criteria}
    }Elseif ($Type -eq "Name"){
        Write-Output $Switch.Ports | Where-Object {$_.Name -like $Criteria}
    }Elseif ($Type -eq "Location"){
        Write-Output $Switch.Ports | Where-Object {$_.Location -like $Criteria}
    }Elseif ($Type -eq "Description"){
        Write-Output $Switch.Ports | Where-Object {$_.Description -like $Criteria}
    }Elseif ($Type -eq "IP"){
        Write-Output $Switch.Ports | Where-Object {$_.IP -like $Criteria}
    }Elseif ($Type -eq "Vlan"){
        Write-Output $Switch.Ports | Where-Object {$_.Vlan -like $Criteria}
    }
}

Function Test-Switch{
    Param([Object[]]$Devices
         )
    foreach ($Device in $Devices) {
        $Res = Test-Connection -Count 1 -Quiet -ComputerName $Device.IP
        $Device.Online = $Res
    }
}

$Panel1 = New-Switch-Object -Name "A00CYA10GN010" -IP "192.168.109.241" -Size 24 -Location "+A00CYA10"
$Panel2 = New-Switch-Object -Name "A00CYA10GN011" -IP "192.168.109.242" -Size 24 -Location "+A00CYA10"
$Panel3 = New-Switch-Object -Name "A00CYR10GN010" -IP "192.168.109.243" -Size 16 -Location "+A00CYR10"
$Panel4 = New-Switch-Object -Name "A00CYR20GN010" -IP "192.168.109.244" -Size 16 -Location "+A00CYR20"
$Panel5 = New-Switch-Object -Name "A00CYR30GN010" -IP "192.168.109.245" -Size 16 -Location "+A00CYR30"
$Panel6 = New-Switch-Object -Name "A00CYR31GN010" -IP "192.168.109.246" -Size 16 -Location "+A00CYR31"
$Panel7 = New-Switch-Object -Name "A00CYR40GN010" -IP "192.168.109.247" -Size 16 -Location "+A00CYR40"
$Panel8 = New-Switch-Object -Name "A00CYR50GN010" -IP "192.168.109.248" -Size 16 -Location "+A00CYR50"
$Panel9 = New-Switch-Object -Name "A00CYR60GN010" -IP "192.168.109.249" -Size 16 -Location "+A00CYR60"
$Panel10 = New-Switch-Object -Name "A00CYR70GN010" -IP "192.168.109.250" -Size 16 -Location "+A00CYR70"

$Site = $Panel1,$Panel2,$Panel3,$Panel4,$Panel5,$Panel6,$Panel7,$Panel8,$Panel9,$Panel10

$Panel1 = Update-Port -Switch $Panel1 -PortNumber 2 -IP "192.168.109.34" -Description "Camera" -Location "Somewhere" -Name "GN303" -Vlan 100,200,242
$Panel1 = Update-Port -Switch $Panel1 -PortNumber 1 -IP "192.168.109.12" -Description "AP" -Location "Elsewhere" -Name "AP001" -Vlan 100
$Panel1 = Update-Port -Switch $Panel1 -PortNumber 3 -IP "192.168.109.23" -Description "Fan" -Location "Roof" -Name "AN001"

Get-Port -Criteria 2 -Switch $Panel1

#Test-Switch $Site




