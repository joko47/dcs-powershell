﻿<#

.SYNOPSIS

Returns the currently logged on user on a client.

.DESCRIPTION

Utilizes the command "quser" to return the currently logged on user from a

remote Client.  This cmdlet will convert the returned strings into PowerShell

objects.  PowerShell remoting must be turned on in your environment for this

command to work.

.PARAMETER ComputerName

The name of the client to connect to.

.EXAMPLE

Get-LoggedOnUser -ComputerName "Tech-cl1"

 

ID          :

SessionName : No Logged On User

Online      : True

LogOnTime   :

IdleTime    :

UserName    : No Logged On User

State       :

 

This example shows that there is currently no user logged on, but the client is

online.

 

.EXAMPLE

Get-LoggedOnUser -ComputerName "Tech-cl1"

 

ID          : 2

SessionName : console

Online      : True

LogOnTime   : 4/25/2014 8:56:00 AM

IdleTime    : none

UserName    : chuck

State       : Active

 

This example demonstrates that the client is logged in to.

 

.EXAMPLE

"Tech-cl1", "tech-ex1", "bad" | Get-LoggedOnUser

 

Recovers the logged in user information from multiple clients.

 

#>

Function Get-LoggedOnUser
{
    [CmdletBinding()]
    Param (
        [parameter(ValueFromPipeline=$true)]
        [String[]]$ComputerName=$env:COMPUTERNAME,
        [switch]$Naming,
        [string[]] $Ignore
    )
    BEGIN
    {
        # Function New-UserSession-Object -----------------------------------------
        # Creates an object to hold the corresponding data from the
        # QUSER command.
        Function New-UserSession-Object
        {
            $Obj = New-Object -TypeName PSObject -Property @{
                'ComputerName' = $null
                'Online' = $True
                'UserName' = "No Logged On User"
                'State' = $null
                'LogOnTime' = $null
                'ID' = $null
                'IdleTime' = $null
                'SessionName' = "No Logged On User"
		'CurrentSession' = $False
                }
            $Obj.psobject.typenames.insert(0,'HDObject.LoggedOnUsers')
            Write-Output $Obj
            } # END: Function New-UserSession-Object ----------------------------------
    $Users = @()
    $ComputerCount = $ComputerName.Count - $Ignore.Count
    if(($MyPowershellProfile -eq "JakobProfile") -and $Naming){$PSProfileFound = $true}Else{$PSProfileFound = $false}
    } # END: BEGIN Block   

    PROCESS
    {
        $counter=0
        $ComputerName = $ComputerName | Where-Object { $Ignore -notcontains $_ }
        ForEach ($C in $ComputerName)
        {
            $Counter+=1
            if($PSProfileFound){Name "Get-LoggedOnUser - Processing $C ($counter/$ComputerCount)"}
            $Obj =  New-UserSession-Object
            # Clear the $DATA variable by setting it to $null.
            $Data = $null
            # Check if run from local computer
            if($C -eq $env:COMPUTERNAME)
            {
                $Data = Quser
            }
            Else
            {
                Try
                {
                    $Data  = Invoke-Command -ComputerName $C -ScriptBlock {
                        $Data = Quser
                        Write-Output $Data
                    } -ErrorAction Stop
               }
               Catch
               {
                    # If "quser" returns an error, then there is no loged on user.
                    # Send the default object values. 
                    #$Obj =  New-UserSession-Object
                    $Obj.ComputerName = $C
                    Try
                    {
                        $TestSession = Test-Connection -ComputerName $C -ErrorAction Stop
                    }
                    Catch
                    {
                       $Obj.Online = $False
                    }
               }
            }
           If ($Data -eq $null)
           {
                Write-output $Obj
           }
                ElseIf (($Data -ne $null) -and ($Data.Count -gt 0))
           {
                For ($X=1;$X -le $Data.Count-1; $X++)
                {
                    $Obj =  New-UserSession-Object
                    $Obj.UserName = $Data[$X].Remove(16).Trim()
                    $Obj.SessionName = $Data[$X].Remove(0,16).Remove(19).Trim()
                    $Obj.ID = $Data[$X].Remove(0,41).Remove(3).Trim()
                    $Obj.State = $Data[$X].Remove(0,44).Remove(8).Trim()
                    $Obj.IdleTime = $Data[$X].Remove(0,54).Remove(11).Trim()
                    $Obj.LogOnTime = Get-Date "$($Data[$X].Remove(0,65).Trim())" -ErrorAction SilentlyContinue
                    $Obj.ComputerName = $C
		    if($Obj.UserName.StartsWith(">")){
		    	$Obj.CurrentSession = $True
			$Obj.UserName = $Obj.UserName.Remove(0,1)
		    }
                    $Users += $Obj
                }
           }
        } # END: ForEach ($C in $ComputerName)
        Write-Output $Users
    } # END: PROCESS Block   
}