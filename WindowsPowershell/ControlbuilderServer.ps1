﻿if($MyPowershellProfile -eq "JakobProfile"){Name "Servers with Control Builder Running - Running"}

$Processes = Get-ProcessCreationTime -ComputerName $Global:ComputerList -ProcessName "ControlBuilderPro.exe"

$cb = @()

$ProcessesCount = $Processes.count

foreach($Process in $Processes){
$x += 1
$Computer = $Process.CSName
if($MyPowershellProfile -eq "JakobProfile"){Name "Servers with Control Builder Running - Processing $Computer ($x/$ProcessesCount)"}
$cb += Get-LoggedOnUser -ComputerName $Process.CSName | Where-Object{$_.UserName -eq $Process.UserName}}

Write-Host ("`n`n                    Servers with Control Builder Running                    ") 
Write-Host ($cb | Sort-object -Property State, ComputerName, UserName | Format-Table -GroupBy State | Out-String)

if($MyPowershellProfile -eq "JakobProfile"){Name "Servers with Control Builder Running - Finished"}