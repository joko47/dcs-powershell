# Change look if powershell is run as administrator 
if ($host.UI.RawUI.WindowTitle -match �Administrator�) {$host.UI.RawUI.BackgroundColor = �DarkRed�; $Host.UI.RawUI.ForegroundColor = �White�}

# Set name of window
$host.UI.RawUI.WindowTitle=�Powershell Jakob�

# Add administrator to name, if administrator
$id = [System.Security.Principal.WindowsIdentity]::GetCurrent() 
$p = New-Object System.Security.Principal.WindowsPrincipal($id) 
if ($p.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)) {$Host.UI.RawUI.WindowTitle = "Administrator: " + $Host.UI.RawUI.WindowTitle}

$PSPath = Split-Path $profile 


$ComputerPath = Join-Path -Path $PSPath -ChildPath "\Computers.txt"
if(Test-Path -Path $ComputerPath){$ComputerList = Get-Content -Path $computerPath}
Else{$NoComputerPath = $True}

# Start notepad
Set-Alias -Name np -Value Notepad
Set-Alias -Name ControlBuilderServer -Value (Join-Path -Path $PSPath -ChildPath "\ControlbuilderServer.ps1")
Set-Alias -Name UpdateComputerList -Value (Join-Path -Path $PSPath -ChildPath "\UpdateComputerList.ps1")
Set-Alias -Name ControlBuilder -Value (Join-Path -Path $PSPath -ChildPath "\ControlBuilder.ps1")
Set-Alias -Name DeleteComputerList -Value (Join-Path -Path $PSPath -ChildPath "\DeleteComputerList.ps1")
Set-Alias -Name Users -Value (Join-Path -Path $PSPath -ChildPath "\Users.ps1")
Set-Alias -Name SoftController -Value (Join-Path -Path $PSPath -ChildPath "\SoftController.ps1")
Set-Alias -Name ReadComputerList -Value (Join-Path -Path $PSPath -ChildPath "\ReadComputerList.ps1")
Set-Alias -Name Help -Value (Join-Path -Path $PSPath -ChildPath "\Help.ps1")
Set-Alias -Name Set-WorkplaceShortcut -Value (Join-Path -Path $PSPath -ChildPath "\Set-WorkplaceShortcut.ps1")
Set-Alias -Name Set-Shortcut -Value (Join-Path -Path $PSPath -ChildPath "\Set-Shortcut.ps1")

$MyPowershellProfile = "JakobProfile"

# List drives
function drives{gdr -p FileSystem |where {$_.free -gt 1MB}}

#Set name of windows
function Name{param([string]$Name) $host.UI.RawUI.WindowTitle=$Name}
function FGColor{param([string]$Color) $host.UI.RawUI.ForeGroundcolor = $Color}
function BGColor{param([string]$Color) $host.UI.RawUI.BackGroundcolor = $Color}

# Import AllowEncryptionOracle workaround module
#import-module Set-CredSSP.psm1
import-module Get-LoggedOnUser.psm1
import-module Get-ProcessCreationTime.psm1
import-module New-Popup.psm1
import-module Disconnect-User.psm1

Write-Host -BackgroundColor Black "Powershell DCS profile v0.9"
Write-Host -BackgroundColor Black "Commands available: ControlBuilder, SoftController, Users, ControlBuilderServer"
if($NoComputerPath){Write-Host -Background Red -Foreground Black 'No "Computers.txt" file found, create on using "UpdateComputerList"'}