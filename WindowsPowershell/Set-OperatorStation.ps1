﻿#Set up operator station

Param([String] $ComputerName = $env:COMPUTERNAME
     )

#Make Sign Out button on desktop
if (Test-Path "C:\Users\Public\Desktop\Sign Out.lnk")
{
    Write-Host -BackgroundColor Black -ForegroundColor Green "Public sign out button found"
    $CreateSignOut = 2
}
Else
{
    Write-Host -BackgroundColor Black -ForegroundColor Red "No public sign out button found"
    Write-Host -NoNewline -ForegroundColor Yellow "Do you want to create a public sign out button?"
    $ReadHost = Read-Host " ( Y / N ) "
}


switch ($ReadHost){
    Y {Write-Host -BackgroundColor Black -ForegroundColor Green "Yes, Creating Public Sign Out button"; $CreateSignOut = 1}
    N {Write-Host -BackgroundColor Black -ForegroundColor Yellow "No, Skipping Public Sign Out button"; $CreateSignOut = 0}
    Default {Write-Host -BackgroundColor Black -ForegroundColor Red "Default, Skipping Public Sign Out button"; $CreateSignOut = -1}
}




$Success = $CreateSignOut
switch ($Success){
    {$_ -gt 0} {Write-Host -BackgroundColor Black -ForegroundColor Green " Operator Station Setup finished! "}
    {$_ -eq 0} {Write-Host -BackgroundColor Black -ForegroundColor Yellow  " Operator Station Setup finished without any action! "}
    {$_ -lt 0} {Write-Host -BackgroundColor Black -ForegroundColor Red  " Operator Station Setup finished! "}
}
switch($CreateSignOut){
    {$_ -gt 1} {Write-Host -BackgroundColor Black -ForegroundColor Green  " - Public Sign out button already existing!"}
    {$_ -eq 1} {Write-Host -BackgroundColor Black -ForegroundColor Green  " - Public Sign out button created!"}
    {$_ -eq 0} {Write-Host -BackgroundColor Black -ForegroundColor Yellow " - Public Sign out not button created!"}
    {$_ -lt 0} {Write-Host -BackgroundColor Black -ForegroundColor Red  " - Public Sign out button creation Failed! (Fault Code: $CreateSignOut)"}
}
