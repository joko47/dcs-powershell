﻿# Create workplace shortcut for auto startup

# Futre additions
# Specific workplace id



Param([Switch] $Global,
      [String] $ComputerName = $env:COMPUTERNAME,
      [String] $UserName = $env:USERNAME,
      [String] $Workplace
     )


$StartFolderPath = $HOME + "\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\afwWorkplaceApplication.lnk"
$StartFolderPathGlobal = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\afwWorkplaceApplication.lnk"

$WorkplaceApplication = "C:\Program Files (x86)\ABB 800xA\Base\bin\AfwWorkplaceApplication.exe"

#If workplace id is given, set to open specific workplace
if(($Workplace.Length) -gt 0)
{
    $WorkplaceArguments = '-W="' + $Workplace +'"'
    }
    else
    {
    $WorkplaceArguments = "-DW"
}

#if ($ComputerName -eq $env:COMPUTERNAME) {$LocalPC = $true}

ForEach($Computer in $ComputerName){ 
    if ($Computer -eq $env:COMPUTERNAME) {$LocalPC = $true}else{$LocalPC = $false}
    if($Global)
    {
        if($LocalPC)
        {
            $WshShell = New-Object -comObject WScript.Shell
            $Shortcut = $WshShell.CreateShortcut($StartFolderPathGlobal)
            $Shortcut.TargetPath = $WorkplaceApplication
            $Shortcut.Arguments = $WorkplaceArguments
            $Shortcut.save()
        }
        else
        {
            Invoke-Command -ComputerName $ComputerName -ScriptBlock{
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($Using:StartFolderPathGlobal)
                $Shortcut.TargetPath = $Using:WorkplaceApplication
                $Shortcut.Arguments = $Using:WorkplaceArguments
                $Shortcut.save()
            }
        }
    }
    else
    {
        if($LocalPC)
        {
            ForEach($User in $UserName)
            {
                $StartFolderPath = "C:\Users\"+ $User + "\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\afwWorkplaceApplication.lnk"
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($StartFolderPath)
                $Shortcut.TargetPath = $WorkplaceApplication
                $Shortcut.Arguments = $WorkplaceArguments
                $Shortcut.save()
            }
        }
        else
        {
            ForEach($User in $UserName)
            {
                Invoke-Command -ComputerName $ComputerName -ScriptBlock{
                    $StartFolderPath = "C:\Users\"+ $Using:User + "\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\afwWorkplaceApplication.lnk"
                    if(!(Test-Path (split-path $StartFolderPath))){Write-Host "Path does not exist, please log in to user once"}
                    $WshShell = New-Object -comObject WScript.Shell
                    $Shortcut = $WshShell.CreateShortcut($StartFolderPath)
                    $Shortcut.TargetPath = $Using:WorkplaceApplication
                    $Shortcut.Arguments = $Using:WorkplaceArguments
                    $Shortcut.save()
                }
            }
        }
    }
}
