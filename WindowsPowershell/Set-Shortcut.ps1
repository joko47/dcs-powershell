﻿# Create logoff shortcut on desktop


Param([Switch] $Global,
      [String] $ComputerName = $env:COMPUTERNAME,
      [String] $UserName = $env:USERNAME,
      [String] $Name = "Sign Out",
      [String] $Application = "C:\Windows\System32\logoff.exe",
      [String] $IconSet = "C:\Windows\System32\shell32.dll",
      [Parameter(HelpMessage="Enter an icon set")]
      [ValidateNotNullorEmpty()]
      [ValidateSet("Standard", "Custom", "LogOff","Reboot","ShutDown")]
      [string] $Icon = "LogOff",
      [String] $Arguments,
      [Parameter(HelpMessage="Enter an WindowStyle set")]
      [ValidateNotNullorEmpty()]
      [ValidateSet("Normal", "Maximized","Minimized")]
      [string] $WindowStyle = "Normal",
      [Parameter(HelpMessage="Enter a location")]
      [ValidateNotNullorEmpty()]
      [ValidateSet("Desktop", "Startup")]
      [string] $Location = "Desktop"
     )


Switch ($Icon) {
    "Standard"       {$IconValue = $null}
    "Custom"         {$IconValue = $null}
    "LogOff"         {$IconValue = 44}
    "Reboot"         {$IconValue = 238}
    "ShutDown"       {$IconValue = 27}
}

Switch ($WindowStyle) {
    "Normal"         {$WindowStyleValue = 4}
    "Maximized"      {$WindowStyleValue = 3}
    "Minimized"      {$WindowStyleValue = 7}
}

#if ($ComputerName -eq $env:COMPUTERNAME) {$LocalPC = $true}

ForEach($Computer in $ComputerName){ 
    if ($Computer -eq $env:COMPUTERNAME) {$LocalPC = $true}else{$LocalPC = $false}
    if($Global)
    {
        if($LocalPC)
        {
            $WshShell = New-Object -comObject WScript.Shell
            $Shortcut = $WshShell.CreateShortcut($PathGlobal)
            $Shortcut.TargetPath = $Application
            $Shortcut.Arguments = $Arguments
            if ($Icon -ne "Standard"){
            $Shortcut.IconLocation = "$IconSet,$IconValue"
            }
            $Shortcut.WindowStyle = $WindowStyleValue
            $Shortcut.save()
        }
        else
        {
            Invoke-Command -ComputerName $ComputerName -ScriptBlock{
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($Using:PathGlobal)
                $Shortcut.TargetPath = $Using:Application
                $Shortcut.Arguments = $Using:Arguments
                if ($Using:Icon -ne "Standard"){
                $Shortcut.IconLocation = "$Using:IconSet,$Using:IconValue"
                }
                $Shortcut.WindowStyle = $Using:WindowStyleValue
                $Shortcut.save()
            }
        }
    }
    else
    {
        if($LocalPC)
        {
            ForEach($User in $UserName)
            {
                Switch ($Location) {
                    "Startup"        {$Path = "C:\Users\$UserName\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\$Name.lnk"}
                    "Dekstop"        {$Path = "C:\Users\$UserName\Desktop\$Name.lnk"}
                }
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($Path)
                $Shortcut.TargetPath = $Application
                $Shortcut.Arguments = $Arguments
                if ($Icon -ne "Standard"){
                $Shortcut.IconLocation = "$IconSet,$IconValue"
                }
                $Shortcut.WindowStyle = $WindowStyleValue
                $Shortcut.save()
            }
        }
        else
        {
            ForEach($User in $UserName)
            {
                Switch ($Location) {
                    "Startup"        {$Path = "C:\Users\$UserName\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\$Name.lnk"}
                    "Dekstop"        {$Path = "C:\Users\$UserName\Desktop\$Name.lnk"}
                }
                Invoke-Command -ComputerName $ComputerName -ScriptBlock{
                    
                    if(!(Test-Path (split-path $Using:Path))){Write-Host "Path does not exist, please log in to user once"}
                    $WshShell = New-Object -comObject WScript.Shell
                    $Shortcut = $WshShell.CreateShortcut($Using:Path)
                    $Shortcut.TargetPath = $Using:Application
                    $Shortcut.Arguments = $Using:Arguments
                    if ($Using:Icon -ne "Standard"){
                    $Shortcut.IconLocation = "$Using:IconSet,$Using:IconValue"
                    }
                    $Shortcut.WindowStyle = $Using:WindowStyleValue
                    $Shortcut.save()
                }
            }
        }
    }
}
