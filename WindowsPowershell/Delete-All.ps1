﻿# Clean up used temporaryu stuff
$CredentialsPath = join-path -Path $Global:PSPath -ChildPath "\SavedCredentials.txt"

# Remove powershell on context menu
# Remove ability to run scripts


# Delete cmdkey entries
$SavedCredentials = Get-Content -Path $CredentialsPath
foreach($Credential in $SavedCredentials)
{
    cmdkey -delete $Credential
}
Remove-Item -Path $CredentialsPath -Force
Write-Host "CMDKeys removed."
cmdkey -list



#Delete powershell scritps
#Remove-Item -Path $GlobalPSPath -Recurse -Force