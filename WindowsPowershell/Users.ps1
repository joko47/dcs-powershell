Param([String] $GroupBy = "ComputerName",
      [String[]] $Ignore
     )

if($MyPowershellProfile -eq "JakobProfile"){Name "Logged on Users - Started"}
$Time = Get-Date -Format "dd/MM-yyyy HH:mm"
$Data = (Get-LoggedOnUser -Computername $Global:ComputerList -Naming -Ignore $Ignore)
$TableData = ($Data | Sort-Object -Property $GroupBy | Where-Object{$_.UserName -ne "No Logged On User"} | Format-Table -AutoSize -GroupBy $GroupBy | Out-String)

$Global:Data = $Data

Write-Host "`n`n          Users on system - $Time       "

ForEach($obj in $Data)
{
    If ($obj.Online -eq $false)
    {
        Write-Host -BackgroundColor Black -ForegroundColor Red "$($Obj.ComputerName) Unreachable"
    }
}
Write-Host $TableData
if($MyPowershellProfile -eq "JakobProfile"){Name "Logged on Users - Finished"}