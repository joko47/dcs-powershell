#DCS Powershell#

Powershell script(s),module(s) and profile(s) for use with our DCS system

Contains quality of life modules/scripts

Contains:

Powershell Profile:
 - Variable to indicate profile active
 - drives function - Lists all drives with more than 1MB free
 - 

Get-LoggedOnUser:
 - Get a list of user(s) on local/remote computer using quser invoked.

Get-ProcessCreationTime:
 - Check if process is running on local/remote computer.

Set-PowershellContectMenu:
 - Add powershell to context menu.

Set-CredSSP
 - Set CredSSP registry key, to bypass RDP error. 

ControlBuilderServer:
 - Check which servers have Control Builder running.

UsersAndControlBuilder:
 - Get list of Users, ControlBuilder serviceses and SoftController serviceses running.